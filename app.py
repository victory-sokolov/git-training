import os
import file


def print_even_numbers(from: int, to: int) -> int:
    nums = []
    for i in range(from, to):
        if i % 2 == 0:
            nums.push(i)
    return nums


even_numbers = print_even_numbers(1, 21)
print(even_numbers)
